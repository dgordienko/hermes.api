using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;

namespace hermes.api.Classes
{
    public sealed class ControllerEventArg:EventArgs{
        public object Result {get;set;}
        public Exception Exception {get;set;}
    }

    public delegate void ControllerHelperEvent(object sender, ControllerEventArg arg);

    public abstract class  ControllerHelper{
    
        public abstract IEnumerable<string> Get();
        public abstract string Get(string config);
        public abstract void Post(string config);
        public abstract void Put(string config);
        public abstract void Delete(string config);

        public  abstract event ControllerHelperEvent GetComplete;
        public abstract event ControllerHelperEvent PutComplete;
        public abstract event ControllerHelperEvent PostComplete;
        public abstract event ControllerHelperEvent DeleteComplete;

                
        ///<summary>
        /// Асинхронное поучение данных из серверного ресурса согласно конфигурации
        ///</summary>
        public virtual async Task<string> HttpClientExecute(string url){
            using(var client = new System.Net.Http.HttpClient())
            {
                using(var r = await client.GetAsync(new Uri(url)))
                {
                    return await r.Content.ReadAsStringAsync();                    
                }
            }
        }
    }

    internal class PointsControllerHelperConfig : IPointsHelperConfig{
        public string ServiceUri{
            get;set;
        }
    }

    public class PointsControllerHelperConfigConverter : CustomCreationConverter<IPointsHelperConfig>
    {
        public override IPointsHelperConfig Create(Type objectType)
        {
            return new PointsControllerHelperConfig();
        }
    }
    public interface IPointsHelperConfig{
        string ServiceUri {get;set;}
    }

    ///<summary>
    /// Вспомагательный класс для работы с точками доставки
    ///</summary>
    public sealed class DeliveryPointsHelper : ControllerHelper,IDisposable
    {
        ///<summary>
        ///Звершение удаления точки доставки
        ///</summary>
        public override event ControllerHelperEvent DeleteComplete;
        void OnDeleteComplete(object sender,ControllerEventArg arg){
            DeleteComplete?.Invoke(sender,arg);
        }
        ///<summary>
        /// Звершение получения информации о точке доставки
        ///</summary>
        public override event ControllerHelperEvent GetComplete;

        void OnGetComplete(object sender,ControllerEventArg arg){
            GetComplete?.Invoke(sender,arg);
        }

        ///<summary>
        /// Звершение добавления информации о точке доставки
        ///</summary>
        public override event ControllerHelperEvent PostComplete;        
        void OnPostComplete(object sender,ControllerEventArg arg){
            PostComplete?.Invoke(sender,arg);
        }

        ///<summary>
        /// Звершение обновления информации о точке доставки
        ///</summary>
        void OnPutComplete(object sender,ControllerEventArg arg){
            PutComplete?.Invoke(sender,arg);
        }
        ///<summary>
        /// Звершение обновления информации о точке доставки
        ///</summary>
        public override event ControllerHelperEvent PutComplete;

        public override void Delete(string config)
        {
            throw new NotImplementedException();
        }

        ///<summary>
        ///  обновления информации о точках доставки
        ///</summary>
        public override IEnumerable<string> Get()
        {
            var config = new PointsControllerHelperConfig();
            config.ServiceUri = @"";            
            throw new NotImplementedException();
        }
        

        public override string Get(string config)
        {
            throw new NotImplementedException();
        }

        public override void Post(string config)
        {
            throw new NotImplementedException();
        }

        public override void Put(string config)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }
                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
        #endregion
    }

}