
using System;
using System.Collections.Generic;

namespace hermes.api.Classes{

    public  interface ITradeAgentsServiceConfig{
        string ServiceUrl {get;set;}
        double Id {get;set;}
        string Date {get;set;}
    }

    internal class TradeAgentsServiceConfig : ITradeAgentsServiceConfig
    {
        public string ServiceUrl
        {
            get;set;
        }
        public double Id {get;set;}
        public string Date {get;set;}
    }

    internal sealed class TradeAgentsService : ControllerHelper,IDisposable
    {
        public override event ControllerHelperEvent DeleteComplete;
        public override event ControllerHelperEvent GetComplete;
        public override event ControllerHelperEvent PostComplete;
        public override event ControllerHelperEvent PutComplete;

        public override void Delete(string config)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<string> Get()
        {
            throw new NotImplementedException();
        }

        public override string Get(string config)
        {
            throw new NotImplementedException();
        }

        public override void Post(string config)
        {
            throw new NotImplementedException();
        }

        public override void Put(string config)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TradeAgentsService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}