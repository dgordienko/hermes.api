using System.Collections.Generic;
using GeoJSON.Net;
using Nancy;
using System;
using Newtonsoft.Json;

namespace hermes.api.Classes
{
    /// <summary>
    /// Основной класс доступа и конфигурации Hermes API 
    /// </summary>
    public class   ApiOwinModule:NancyModule{
        public ApiOwinModule(){
          
            Get("/api/", args => "Hermes public API");    
            //Информация о всех точках доставки        
            Get("/api/03",args=>{
                return GetDeliveryPoints(args);
            });
            //Информация о конкретной точке доставки
            Get("/api/03/{code}",args=>{                
                return GetDeliveryPoint(args);
            });

            //Все маршруты грузового автотраспорта 
            Get("api/01/{config}",args=>{                    
                    throw new NotImplementedException();
                }); 

            //Маршруты грузового автотраспорта согласно идентификатору и конфигурации       
            Get("api/01/{id}/{config}",args=>{
                    throw new NotImplementedException();
                });

            //Точки доставки грузового автотраспорта согласно конфигурации
            Get("api/01/{id}/03/{config}",args=>{
                    return GetFleetDeliveryPoints(args.id,args.config);
                });

            //Все маршруты транспортных средств торговых агентов
            Get("api/02/{config}",args=>{
                 throw new NotImplementedException();
                });  
            //Маршруты траспортного средства ТА идентификатору и конфигурации       
            Get("api/02/{id}/{config}",args=>{
                    throw new NotImplementedException();
                });
             //Точки доставки из маршрутов ТА идентификатору и конфигурации 
            Get("api/02/{id}/03/{config}",args=>{
                    return GetAgentsDeliveryPoints(args.id,args.config);
                });
            //Редактирование информации о точке доставки
            Post("api/03/post/{config}",args=>{
                throw new NotImplementedException();
            });
            //Удаление информации о точке доставки
             Delete("api/03/delete/{config}",args=>{
                 throw new NotImplementedException(); 
             });
        }
        /// <summary>
        /// Получение данных о точке доставки
        /// </summary>
        /// <param name="args">Аргументы</param>
        /// <returns>IGeoJSONObject</returns>
        private static IGeoJSONObject GetDeliveryPoint(dynamic args){
            IGeoJSONObject result = null;
            using (var helper = new DeliveryPointsHelper()){
                helper.GetComplete +=(sender,arg)=>{
                    arg.With(x=>x.Exception.Do(ex=>{
                        throw ex;
                    }));
                    arg.With(x=>x.Result.Do(res=>{
                    result = ((IGeoJSONObject)res);
                    }));
                };
                helper.Get(args.code);
            }
            return result;
        }
        /// <summary>
        /// Получение данных о точках доставки
        /// </summary>
        /// <param name="args">Аргументы</param>
        /// <returns>IEnumerable(IGeoJSONObject)></returns>
        private static  IEnumerable<IGeoJSONObject> GetDeliveryPoints(string args)
        {
            IEnumerable<IGeoJSONObject> result = null;
            using(var helper = new DeliveryPointsHelper()){                
                helper.GetComplete +=(sender,arg)=>{
                    arg.With(x=>x.Result.Do(r=>{
                        result = ((IEnumerable<IGeoJSONObject>)r);
                    }));
                    arg.With(x=>x.Exception.Do(e=>{
                        throw e;
                    })) ;                   
                };
                helper.Get();                
            }            
            return result;
        }

        /// <summary>
        /// Возвращает точки доставки грузового автотраспорта
        /// </summary>
        /// <param name="config">Конфигурация</param>
        /// <returns>IEnumerable(IGeoJSONObject)></returns>
        private static IEnumerable<IGeoJSONObject> GetFleetDeliveryPoints(double id,string config){
            IEnumerable<IGeoJSONObject> result = null;
            using(var helper = new FleetService()){
                helper.GetComplete += (sender,arg)=>{
                    arg.With(x=>x.Result.Do(res=>{
                        result = (IEnumerable<IGeoJSONObject>)res;
                    }));
                    arg.With(x=>x.Exception.Do(ex=>{
                        throw ex;
                    }));
                };
                var cfg = new FleetServiceConfig();
                helper.Get(JsonConvert.SerializeObject(cfg));
            }
            return result;
        }
        /// <summary>
        /// Возвращает точки доставки ТА
        /// </summary>
        /// <param name="config">Конфигурация</param>
        /// <returns>IEnumerable(IGeoJSONObject)></returns>
        private static IEnumerable<IGeoJSONObject> GetAgentsDeliveryPoints(double id,string config){
            IEnumerable<IGeoJSONObject> result = null;
            using(var helper = new TradeAgentsService()){
                helper.GetComplete += (sender,arg)=>{
                    arg.With(x=>x.Result.Do(res=>{
                        result = (IEnumerable<IGeoJSONObject>)res;
                    }));
                    arg.With(x=>x.Exception.Do(ex=>{
                        throw ex;
                    }));
                };
                var cfg = new TradeAgentsServiceConfig();
                helper.Get(JsonConvert.SerializeObject(cfg));
            }
            return result;
        }
    }
}